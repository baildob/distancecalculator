# Distance Calculator

**What sort(s) of things would you do differently if you were implementing this functionality in a production website that received a very high amount of traffic?** 

1. Read the file contents only once on startup
2. Cache the contents of the file once it has been read
3. Split out the API into a separate repo/solution with unit tests. This would allow it to be deployed independently. The API could then be deployed to the cloud and setup to auto-scale under defined load conditions.
4. Improve validation & error handling

**Document any assumptions you have made:**

1. Using Google Distance Matrix to calculate the driving distance rather than doing an "as the crow files" method to calculate the distance




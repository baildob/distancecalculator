using AddressSearch;
using FileParser;
using GoogleDistanceMatrix;
using GoogleDistanceMatrix.Models;
using Microsoft.Extensions.Options;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace DistanceCalculator.Services.UnitTests
{
    public class AddressSearchServiceUnitTests
    {
        [Fact]
        public void TestExpectedCount()
        {
            // Moq dependencies
            IOptions<AddressSearch.Models.Config> config = Options.Create<AddressSearch.Models.Config>
            (
                new AddressSearch.Models.Config() { Filename = "", Top = 5 }
            );

            string destination = "500 Collins Street, Melbourne VIC 3000";

            string[] fileAddressList = new string[]
            {
                "2 Olympic Street, Mansfield VIC 3722",
                "50 Opal Street, Lightning Ridge NSW 2834",
                "29 Eyre Street, Charleville QLD 4470",
                "61 Edington Street, Berserker QLD 4701",
                "7 Romeo Street, Mackay QLD 4740",
                "20B Reynolds Street, Nebo QLD 4742",
                "41 Bellevue Drive, Port Macquarie NSW 2444",
                "74 Severn Street, Deepwater NSW 2371"
            };

            var moqTextFileParser = new Mock<ITextFileParser>();
            moqTextFileParser.Setup(a => a.Parse(It.IsAny<string>())).Returns(fileAddressList);

            var moqDistanceMatrix = new Mock<IDistanceMatrix>();
            moqDistanceMatrix.Setup(x => x.GetDistanceAsyncAsJson(It.IsNotNull<DistanceMatrixRequest>())).Returns(
                Task.FromResult(new DistanceMatrixResponse()
                {
                    status = "OK",
                    destination_addresses = new string[] { destination },
                    origin_addresses = fileAddressList,
                    rows = new Row[] {
                        new Row { elements = new Element[] { new Element() { distance = new Distance() { text = "20", value = 20 } } } },
                        new Row { elements = new Element[] { new Element() { distance = new Distance() { text = "15", value = 15 } } } },
                        new Row { elements = new Element[] { new Element() { distance = new Distance() { text = "8", value = 8 } } } },
                        new Row { elements = new Element[] { new Element() { distance = new Distance() { text = "2", value = 2 } } } },
                        new Row { elements = new Element[] { new Element() { distance = new Distance() { text = "6", value = 6 } } } },
                        new Row { elements = new Element[] { new Element() { distance = new Distance() { text = "10", value = 10 } } } },
                        new Row { elements = new Element[] { new Element() { distance = new Distance() { text = "1", value = 1 } } } },
                        new Row { elements = new Element[] { new Element() { distance = new Distance() { text = "25", value = 25 } } } }
                    }
                }));

            // Verify
            var service = new AddressSearchService(config, moqTextFileParser.Object, moqDistanceMatrix.Object);
            var response =  service.GetNearestAddresses(new AddressSearch.Models.AddressSearchRequest() { Destination = destination });

            Assert.True(response.Results.Count == 5); // filtered to 5 results
            Assert.True(response.Results[0].DistanceValue == 1); // nearest result in the 1st position
        }
    }
}

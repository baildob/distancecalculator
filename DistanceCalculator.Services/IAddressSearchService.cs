﻿using AddressSearch.Models;

namespace AddressSearch
{
    public interface IAddressSearchService
    {
        AddressSearchResponse GetNearestAddresses(AddressSearchRequest request);
    }
}

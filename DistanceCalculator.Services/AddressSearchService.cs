﻿using AddressSearch.Models;
using FileParser;
using GoogleDistanceMatrix;
using GoogleDistanceMatrix.Models;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AddressSearch
{
    public class AddressSearchService : IAddressSearchService
    {
        private readonly IOptions<Models.Config> Config;
        private readonly ITextFileParser AddressFileParser;
        private readonly IDistanceMatrix DistanceMatrix;

        public AddressSearchService(IOptions<Models.Config> config, ITextFileParser addressFileParser, IDistanceMatrix distanceMatrix)
        {
            Config = config;
            AddressFileParser = addressFileParser;
            DistanceMatrix = distanceMatrix;
        }

        public AddressSearchResponse GetNearestAddresses(AddressSearchRequest request)
        {
            // Read the contents of the address file
            string directory = System.AppContext.BaseDirectory;
            string filename = Config.Value.Filename;
            string path = Path.Combine(directory, filename);
            string[] lines = AddressFileParser.Parse(path);

            // Call the Google Distance Matrix API
            DistanceMatrixResponse distanceMatrixResponse = DistanceMatrix.GetDistanceAsyncAsJson(new DistanceMatrixRequest()
            {
                Origins = lines.ToList(),
                Destinations = new List<string>() { request.Destination }
            }).Result;

            return BuildAddressSearchResponse(distanceMatrixResponse);
        }

        private AddressSearchResponse BuildAddressSearchResponse(DistanceMatrixResponse distanceMatrixResponse)
        {
            AddressSearchResponse response = new AddressSearchResponse();

            List<AddressDetail> allAddresses = new List<AddressDetail>();

            if (distanceMatrixResponse.status.ToUpper().CompareTo("OK") == 0 && 
                distanceMatrixResponse.rows.Length > 0 &&
                distanceMatrixResponse.destination_addresses.Length > 0)
            {
                for (int i = 0; i < distanceMatrixResponse.origin_addresses.Length; i++)
                {
                    allAddresses.Add(new AddressDetail()
                    {
                        Address = distanceMatrixResponse.origin_addresses[i],
                        DistanceValue = distanceMatrixResponse.rows[i].elements[0].distance.value,
                        DistanceText = distanceMatrixResponse.rows[i].elements[0].distance.text
                    });
                }

                response.Results = allAddresses.OrderBy(a => a.DistanceValue).Take(Config.Value.Top).ToList();
            }

            return response;
        }
    }
}

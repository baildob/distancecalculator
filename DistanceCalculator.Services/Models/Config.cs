﻿namespace AddressSearch.Models
{
    public class Config
    {
        public string Filename { get; set; }

        public int Top { get; set; }
    }
}

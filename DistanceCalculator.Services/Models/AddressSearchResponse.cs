﻿using System.Collections.Generic;

namespace AddressSearch.Models
{
    public class AddressSearchResponse
    {
        public AddressSearchResponse()
        {
            Results = new List<AddressDetail>(); 
        }

        public List<AddressDetail> Results { get; set; }
    }

    public class AddressDetail
    {
        public string Address { get; set; }
        public int DistanceValue { get; set; }
        public string DistanceText { get; set; }
    }
}

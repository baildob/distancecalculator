﻿namespace AddressSearch.Models
{
    public class AddressSearchRequest
    {
        public string Destination { get; set; }
    }
}

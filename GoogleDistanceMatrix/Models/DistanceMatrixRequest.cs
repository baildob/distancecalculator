﻿using System.Collections.Generic;

namespace GoogleDistanceMatrix.Models
{
    public class DistanceMatrixRequest
    {
        public List<string> Origins { get; set; }

        public List<string> Destinations { get; set; }
    }
}

﻿namespace GoogleDistanceMatrix.Models
{
    public class Config
    {
        public string ServerUrl { get; set; }

        public string ApiKey { get; set; }
    }
}

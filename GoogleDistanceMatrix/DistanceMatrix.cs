﻿using GoogleDistanceMatrix.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace GoogleDistanceMatrix
{
    public class DistanceMatrix : IDistanceMatrix
    {
        private readonly IOptions<Config> Config;
        private static HttpClient HttpClient = new HttpClient();

        public DistanceMatrix(IOptions<Config> config)
        {
            this.Config = config;
        }

        public async Task<DistanceMatrixResponse> GetDistanceAsyncAsJson(DistanceMatrixRequest request)
        {
            DistanceMatrixResponse response = null; 

            string url = string.Concat($"{Config.Value.ServerUrl}/json?",  
                $"key={Config.Value.ApiKey}",
                $"&origins={string.Join("|", request.Origins)}",
                $"&destinations={string.Join("|", request.Destinations)}");

            HttpResponseMessage httpResponse = await HttpClient.GetAsync(url);

            bool success = httpResponse.IsSuccessStatusCode;
            if (success)
            {
                string json = await httpResponse.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<DistanceMatrixResponse>(json);
            }

            return response;
        }
    }
}

﻿using GoogleDistanceMatrix.Models;
using System.Threading.Tasks;

namespace GoogleDistanceMatrix
{
    public interface IDistanceMatrix
    {
        Task<DistanceMatrixResponse> GetDistanceAsyncAsJson(DistanceMatrixRequest request);
    }
}

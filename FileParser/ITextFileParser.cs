﻿namespace FileParser
{
    public interface ITextFileParser
    {
        string[] Parse(string path);
    }
}

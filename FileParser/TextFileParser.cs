﻿using System.IO;

namespace FileParser
{
    public class TextFileParser : ITextFileParser
    {
        public string[] Parse(string path)
        {
            string[] response = null;

            if (File.Exists(path))
            {
                response = File.ReadAllLines(path);
            }

            return response;
        }
    }
}

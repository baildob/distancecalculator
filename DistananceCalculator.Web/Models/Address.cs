﻿namespace DistanceCalculator.Web.Models
{
    public class Address
    {
        public string AddressLine { get; set; }

        public double Distance { get; set; }
    }
}

﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AddressSearch;
using AddressSearch.Models;

namespace DistanceCalculator.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressListController : ControllerBase
    {
        private readonly IAddressSearchService AddressSearchService;

        public AddressListController(IAddressSearchService addressSearchService)
        {
            AddressSearchService = addressSearchService;
        }

        [HttpGet]
        public List<AddressDetail> Get(string input)
        {
            var request = new AddressSearchRequest() { Destination = input };

            var response = AddressSearchService.GetNearestAddresses(request);

            return response.Results;
        }
    }
}
